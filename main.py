# -*- coding: utf-8 -*-
import os
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
#from KaggleWord2VecUtility import KaggleWord2VecUtility
import pandas as pd
import numpy as np
import glob
import json

if __name__ == '__main__':
 file_list = glob.glob('./data/*.txt')
 f = open('data.json','w')
 for file_name in file_list:
     Fin = open(file_name,'r')
     temp_text = Fin.read()
     base = os.path.basename(file_name)
     if base[1] == 't':
         sentiment = "0"
     else:
         sentiment = "1"
     temp_data = { 'ID' : base , 'sentiment' : sentiment , 'review' : temp_text }
     out = json.dumps(temp_data)
     f.write(out + '\n')
     Fin.close()

 #g = open('data.json','r')
 #test = json.load(g)
 #print test
 #for line in open('data.json','r'):
 ###### yield eval(1)